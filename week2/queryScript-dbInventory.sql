SELECT * FROM Product;

SELECT * FROM Customer;

SELECT * FROM Order_Line WHERE order_id = 2;

SELECT t1.* FROM Customer_Order as t1
	INNER JOIN Order_Line as t2 
		ON t2.order_id = t1.id
			WHERE t2.id = 5;

SELECT t1.* FROM Customer_Order as t1 
	INNER JOIN Order_Line as t2 
		ON t1.id = t2.order_id
		INNER JOIN Product as t3
			ON t3.id = t2.product_id
			WHERE t3.id = 19;

SELECT id, SUM(total) FROM Order_Line WHERE product_id = 19 GROUP BY id;

UPDATE Product SET price = price * 1.5 WHERE id = 1;

SELECT t1.name FROM Customer as t1
	INNER JOIN Customer_Order as t2
		ON t1.id = t2.customer_id
		INNER JOIN Order_Line as t3
		ON t2.id = t3.order_id
			INNER JOIN Product as t4
				ON t3.product_id = t4.id
				WHERE t4.id = 19;

SELECT * FROM Customer_Order WHERE order_date BETWEEN '2018-01-01' AND '2018-12-31';

SELECT name FROM Product WHERE price > 4.5;

SELECT t1.first_name, t1.last_name, t4.name FROM Customer as t1
	INNER JOIN Customer_Order as t2
		ON t1.id = t2.customer_id 
		INNER JOIN Order_Line as t3
		ON t2.id = t3.order_id
			INNER JOIN Product as t4
				ON t3.product_id = t4.id
				WHERE t1.id = 1;

SELECT t1.id, t1.first_name, t1.last_name, COUNT(t4.name) FROM Customer as t1
	INNER JOIN Customer_Order as t2
		ON t1.id = t2.customer_id 
		INNER JOIN Order_Line as t3
		ON t2.id = t3.order_id
			INNER JOIN Product as t4
				ON t3.product_id = t4.id
				WHERE t1.id = 1 AND
				t2.order_date BETWEEN '2018-01-01' AND '2018-12-31'
				GROUP BY t1.id;

--SELECT * FROM (SELECT t1.product_id as p, SUM(quantity) as s FROM Order_Line as t1 GROUP BY t1.product_id) as t 
--	WHERE t.s = (SELECT MAX(s) FROM (SELECT t1.product_id as p, SUM(quantity) as s FROM Order_Line as t1 GROUP BY t1.product_id) as f);
WITH sumquantity as (
	SELECT t1.product_id as p, SUM(quantity) as s FROM Order_Line as t1 GROUP BY t1.product_id
)
SELECT * FROM sumquantity WHERE sumquantity.s = (SELECT MAX(s) FROM sumquantity); 

DELETE FROM Customer_Order WHERE id = 1;