require 'faker'

f = File.open('insertScript-dbInventory.sql','a')
20.times do |i|
	f.write("INSERT INTO Product (id, sku, name, description, price, stock) VALUES (#{i+1}, \"#{rand(100000..1000000)}\", \"#{Faker::Commerce.product_name}\", \"#{Faker::Books::Lovecraft.paragraph}\", #{Faker::Commerce.price}, #{rand(1..1000)})\n")
end