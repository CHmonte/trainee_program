# frozen_string_literal: true

require_relative 'car.rb'
require_relative 'truck.rb'
require_relative 'store.rb'

s = Store.new
new_car = Car.new('4', 'blue', 'Honda', 12.04)
s.add_vehicles(new_car)
s.add_vehicles(Car.create_five)
s.list_vehicles
s.remove_vehicle(new_car.id)
puts "\n"
s.list_vehicles
puts "\n"
extras = []
3.times do
  extras << VehicleExtra.extra_generator
end
s.vehicle_specs(new_car, extras)
