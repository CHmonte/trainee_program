# frozen_string_literal: true

require 'faker'
require 'securerandom'
require_relative 'vehicle_extra.rb'

# Vehicle class
class Vehicle
  attr_reader :id, :wheels_number, :color, :brand, :price

  def initialize(wheels_number, color, brand, price)
    @id = SecureRandom.uuid
    @wheels_number = wheels_number
    @color = color
    @brand = brand
    @price = price
    @extra = []
  end

  def self.create_five
    vehicles_array = []
    5.times do
      vehicles_array << new(rand(2..18), Faker::Vehicle.color,
                            Faker::Vehicle.make,
                            Faker::Commerce.price(5_000..100_000))
    end
    vehicles_array
  end

  def add_extra(extra)
    return p 'No es un extra' unless extra.instance_of?(VehicleExtra)

    @extra << extra
  end

  def remove_extra(extra)
    return p 'No es un extra' unless extra.instance_of?(VehicleExtra)

    @extra.delete(extra)
  end

  def list_extras
    puts "Extras:\n"
    @extra.each(&:print_extra)
  end

end
