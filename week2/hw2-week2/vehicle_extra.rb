# frozen_string_literal: true

require 'faker'

# class for the vehicle additions
class VehicleExtra
  attr_reader :name, :price

  def initialize(product_name, price)
    @name = product_name
    @price = price
  end

  def self.extra_generator
    new(Faker::Commerce.product_name, Faker::Commerce.price)
  end

  def print_extra
    puts "\t #{name} (#{price})"
  end
end
