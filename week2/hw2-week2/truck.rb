# frozen_string_literal: true

require_relative 'vehicle.rb'
require 'faker'

# Truck class
class Truck < Vehicle
  def initialize(wheels_number, brand, color, price)
    super(wheels_number, brand, color, price)
  end
end
