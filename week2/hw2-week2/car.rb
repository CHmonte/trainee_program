# frozen_string_literal: true

require_relative 'vehicle.rb'
require 'faker'

# class car
class Car < Vehicle
  def initialize(wheels_number, brand, color, price)
    super(wheels_number, brand, color, price)
  end
end
