# frozen_string_literal: true

require_relative 'vehicle.rb'

# Class Store with the hability to list, add and remove vehicles
class Store
  def initialize
    @vehicles = {}
  end

  def list_vehicles
    @vehicles.each_value do |value|
      puts "Vehicle with id: #{value.id} and brand #{value.price}"
    end
  end

  def add_vehicles(data)
    d = data.is_a?(Array) ? data : [data]
    d.each do |v|
      return puts 'No son carros' unless v.is_a?(Vehicle)

      @vehicles.store(v.id, v)
    end
  end

  def remove_vehicle(id)
    @vehicles.delete(id) { |e| puts "Vehicle with id: #{e} not found" }
  end

  def vehicle_specs(car, extras)
    return 'Unsupported values' unless car.is_a?(Vehicle) && extras.is_a?(Array)

    puts "Details:\n\tQuote for the car: #{car.id}\n\tFeatures"
    puts "\tcolor: #{car.color}\tbrand: #{car.brand}"
    puts "\twheels: #{car.wheels_number}\tprice: $#{car.price}"
    puts '-' * 30 + "\nExtras:\n"
    total = extras.inject(0) do |sum, extra|
      return puts 'Unsupported values' unless extra.is_a?(VehicleExtra)

      extra.print_extra
      sum + extra.price
    end
    puts '-' * 30 + "\nTotal: $#{total + car.price}"
  end
end
