CREATE TABLE Customer (
	id INTEGER PRIMARY KEY,
	first_name VARCHAR (300),
	last_name VARCHAR (300),
	address VARCHAR (500),
	phone VARCHAR (150) 
);

CREATE TABLE Product (
	id INTEGER PRIMARY KEY,
	sku VARCHAR (300),
	name VARCHAR (300),
	description VARCHAR (900),
	price NUMERIC,
	stock INTEGER
);

CREATE TYPE state AS ENUM ( 'Delivered', 'On the way', 'Ordered', 'Cancelled');

CREATE TABLE Customer_Order (
	id INTEGER PRIMARY KEY,
	order_date DATE,
	customer_id INTEGER REFERENCES Customer(id),
	total NUMERIC,
	status state
);

CREATE TABLE Order_Line (
	id INTEGER PRIMARY KEY,
	order_id INTEGER REFERENCES Customer_Order(id) ON DELETE CASCADE,
	product_id INTEGER REFERENCES Product(id),
	quantity INTEGER,
	price NUMERIC,
	total NUMERIC
);