class Array
  def to_histogram
    histogram = inject(Hash.new(0)) do |arry_of_frecuency, object_of_array|
      arry_of_frecuency[object_of_array] += 1
      arry_of_frecuency
    end
  end
end

array_of_objects = [1, 2, "2", "2", 3, 3]
p array_of_objects.to_histogram
