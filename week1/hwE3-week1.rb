require_relative 'hwE2-week1.rb'

def mode(array)
  return "Input an array to obtain the mode" unless array.kind_of?(Array)
  histogram = array.to_histogram
  mode_value =  []
  mode_counter = 0
  histogram.each do |key, value|
    if value > mode_counter
   	  mode_counter = value 
  	  mode_value = [key] 
  	elsif value == mode_counter
  	  mode_value << key
  	end 
  end
  return mode_counter > 1 ? mode_value : "There is no mode"
end

def mean(array)
  return "Input an array to obtain the mean or average" unless (array.kind_of?(Array) && !array.empty?) 
  array.inject(0) { |sum, value| sum += value }/array.size.to_f
end

def median(array)
  array.sort!
  median_pos = array.size/2
  return array.size % 2 == 1 ? array[median_pos] : mean(array[median_pos - 1..median_pos]) 
end

p mode([1, 2, 2, 3, 4, 5, 4, 1])
p mean([0, 5, 5, 5, 10])
p median([1, 2, 3, 4, 5])
p median([3, 5, 4, 2, 6, 1])
