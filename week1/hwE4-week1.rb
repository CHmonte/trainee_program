def fix_file(file_dir)
	return "That file does not exits" unless File.file? file_dir
  open(file_dir) do |f| 
  	f.read
  		.gsub(/.\s+/) do |m| 
  	    if m == ".\n\n" || m == ":\n\n" || m == ".\n"
  	    	m
  	    else
  	    	m.strip + " "
  	    end
  	  end
  	  .gsub(/ \./, ".").gsub(/\.\./, ". ")
  end
end

fixed_file = fix_file("strings.txt")
open('Fixed_file','w') {|f| f << fixed_file }
puts "Words that begin with Dis/dis: #{fixed_file.scan(/(dis\w+)/i).size}"
puts "Words that end with ing: #{fixed_file.scan(/\w+ing/).size}"
