require 'csv'
require 'date'
require 'time'

def obtain_dates(file_dir) 
  return "That file does not exits" unless File.file? file_dir
  CSV.read(file_dir)
end

def make_graph(file, first_size, second_size)
	file << []
  tmp_array1 = []
  tmp_array2 = [] 
  tmp_array3 = []
  tmp_array1  << "Frequency"
  range = 0..(first_size >= second_size ? first_size : second_size)
  range.each { |i| tmp_array1 << i }
  file << tmp_array1
  tmp_array2 << "" 
  tmp_array2 << "Valid"
  range = 0..first_size - 1
  range.each { |i| tmp_array2 << "/////"}
  file << tmp_array2
  file << []
  tmp_array3 << "" 
  tmp_array3 << "Invalid"
  range = 0..second_size - 1
  range.each { |i| tmp_array3 << "/////"}
  file << tmp_array3
end

ENV['TZ'] = 'EST'
dates = obtain_dates("events.csv")
range_dates = 0..dates.size - 1
array_of_dates = range_dates.inject([[],[]]) do |result_dates, i| 
																								begin
 			      																			result_dates[0] << [i, Time.parse(dates[i][1]).to_s]
  			  																			rescue ArgumentError
  			  	  																		puts "Invalid date at line #{i + 1}"
  			  	  																		result_dates[1] << [i, dates[i][1]]
  			  																			end
  			  																			result_dates
			  																			end
valid_dates = array_of_dates[0].sort_by{ |pair| pair[1] }
invalid_dates = array_of_dates[1]
data = CSV.generate do |csv|
											valid_dates.each { |element| csv << element }
  										make_graph(csv,valid_dates.size,invalid_dates.size)
       							end
File.write("Valid_Dates.csv", data)
data = CSV.generate { |csv| invalid_dates.each { |element| csv << element } }
File.write("Invalid_Dates.csv", data)
